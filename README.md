# chrono.F90 #
## Simple nested timers ##

The chrono.F90 module provides the Chrono type, a convenient interface to add
nested timers to a code. Its aim is to provide in-program timing reports, and
to be used for simple profiling.

Chrono is very simple to use:

- Initialize the `bigben` timer.
- Start timing a new section with `bigben%split()`.
- Stop timing a section with `bigben%stop()`.
- To create nested timers, simply call `bigben%split()` before calling `bigben%stop()`.
- Remember to stop all sections.
- Get your output in a pretty format (`bigben%split()`) or as a JSON file (`bigben%dump()`).

The following piece of code illustrates how it is used:

    program chrono_example
        use Chrono_class
        implicit none

        bigben=Chrono("chrono_test")
        ! Indentation added to facilitate visualization
            call bigben%split("A")
                call bigben%split("B")
                    call bigben%split("C")
                    call bigben%stop
                    call bigben%split("D")
                        call bigben%split("E")
                        call bigben%stop
                    call bigben%stop
                call bigben%stop
                call bigben%split("F")
                call bigben%stop
            call bigben%stop
            call bigben%split("G")
            call bigben%stop
        call bigben%stop
        call bigben%print()

        call bigben%destroy()
    end program

The output produced (by `bigben%print()`) will look like:

     ╚═chrono_test (.000 s)
         ╠═A (.000 s, 54.1%)
         ║   ╠═B (.000 s, 60.0%)
         ║   ║   ╠═C (.000 s, 11.3%)
         ║   ║   ╚═D (.000 s, 37.6%)
         ║   ║       ╚═E (.000 s, 26.1%)
         ║   ╚═F (.000 s, 5.9%)
         ╚═G (.000 s, 3.2%)
